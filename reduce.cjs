
// // const items = [1, 2, 3, 4, 5, 5];



function reduce(elements, callback, startingValue){
    if (!Array.isArray(elements) || elements.length === 0 || typeof(callback) !== "function") {
        return ;
    }
    let index=0;
    if (startingValue == undefined) {
        startingValue = elements[0];
        index++
    }

    for(;index<elements.length;index++) {
        // console.log(index);
        startingValue = callback(startingValue, elements[index],index,elements);
    }

    return startingValue;


}



    


module.exports = reduce;