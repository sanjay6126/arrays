// const items = [1, 2, 3, 4, 5, 5];


// function cb(number,index){
//     console.log( `index ${index +1} is ${number}`);
// }




function each(items,callback){

    if (!Array.isArray(items) || items.length == 0) {
        return 0;
    }

    if(Array.isArray(items)){
        for(let index=0;index<items.length;index++){
            callback(items[index],index);
        }
    }
    
    
}


// each(cb)






module.exports = each;
