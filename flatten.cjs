
const items = [1, [2], [[3]], [[[4]]]];

function flatten(item, depth = 1) {
    if (item.length == 0) {
        return [];
    }

    let flatArray = [];
    for (let index = 0; index < item.length; index++) {
        if (Array.isArray(item[index]) && depth >= 1) {
            flatArray = flatArray.concat(flatten(item[index], depth - 1));
        }
        else {
            if (item[index] === undefined || item[index] === null) {

                continue;
            }
            else {
                flatArray.push(item[index]);
            }


        }
    }
    return flatArray;
}

module.exports = flatten;

  
  
//   const a = [1, [2], [[3]], [[[4]]]];
//   console.log(flatten(a,level)); // [1, 2, 3, 4, 5, 6, 7];
  
  //when counter variable level ke equal ho jayga tab muje flat variable me push kar dena hoga 0
  
  
  
    
    
//     for (let index = 0; index < items.length; index++) {
//         if (Array.isArray(items[index])) {
//             flatArray = flatArray.concat(flatten(items[index]));
//         }
//         else {
//             flatArray.push(items[index]);
//         }
//     }
//     return flatArray;
// }










module.exports = flatten;