


// function cb(number,index){
//     // console.log( `index ${index +1} is ${number}`);
//     var res = [];
//     res.push()
// }




function map(elements,cb){
    let res = [];
    if (!Array.isArray(elements) || elements.length == 0 || typeof(cb)!=="function") {
        return res;
    }


    for(let index=0;index<elements.length;index++){
        res.push(cb(elements[index],index,elements));
    }

    return res;
    
}


// map(cb)



module.exports = map;
